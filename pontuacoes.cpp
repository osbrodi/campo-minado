#include "pontuacoes.h"
#include "utils.h" 
#include <fstream>
#include <iostream> //cin, cout
#include <stdio.h> //fgets
#include <cstring> //strlen
#include <stdlib.h>	//exit

using namespace std;

ranking* leRanking(const Opcoes& op, int &tam)
{
    //abrir o arquivo para leitura
    ifstream arq("ranking.dat",ios::binary);
    //se falhar ao abrir o arquivo
    if(arq.fail())
    {
        cout << "\nAinda não existe colocados no ranking." << endl;
        return NULL;
    }
    //move para o final do arquivo
    arq.seekg(0,arq.end);
    //obtem o numero de rankings armazenados no arquivo.
    //tellg retorna posição atual no arquivo, como se encontra
    //no final do arquivo retorna seu tamanho, dividindo pelo
    //tamanho de ranking obtém-se o numero de rankings
    tam = arq.tellg() / sizeof(ranking);
    //move para o inicio do arquivo
    arq.seekg(0,arq.beg);
    //aloca o vetor de rankings dinamicamente
    ranking* rank = new ranking[tam];
    //armazena temporariamente os dados lidos para comparação
    ranking r;
    tam = 0;
    //le no arquivo ranking por ranking e apenas armazena 
    //em rank se for do modo de jogo desejado 
    while(arq.read((char*) &r, sizeof(r)))
    {
        if((r.tamanho == op.tam) && (r.modo == op.modo))
        {
            rank[tam++] = r;
        }
    }
    //fecha o arquivo
    arq.close();
    return rank;
}

void salvaRanking(const ranking& rank, const Opcoes& op)
{
    int tam;
    //le o ranking salvo para verificar se já existe
    ranking* vRank = leRanking(op, tam);
    //se o arquivo de ranking não existe, ou não existir 
    //salvo um ranking igual ao atual, ele será salvo
    if((vRank == NULL) || (!achouRanking(vRank, tam, rank)))
    {
        //ios::app para escrever no final do arquivo 
        ofstream arq("ranking.dat",ios::binary | ios::app);
        //se houver falha ao abrir o ranking imprime mensagem de erro
        if(arq.fail())
        {
            cout << "Nâo foi possível salvar o ranking."<< endl;
            exit(1);
        }
        //salva o ranking no arquivo 
        arq.write((char*) &rank, sizeof(rank));
        arq.close();
    }
}

ranking* novoRanking(const Opcoes& op, int nJogadas)
{
    //aloca um ranking dinamicamente
	ranking* r = new ranking;
	cout << "Digite seu nome: ";
    //limpa o buffer para que o fgets não pegue um \n que 
    //poderia ser deixado no buffer por um 'cin' 
    limparBuffer();
    //le uma cadeia de chars digitada pelo jogador
	fgets(r->nome, 50, stdin);
    //obtem a posição do ultimo char do nome
	int ult = strlen(r->nome) - 1;
    //se a ultima posição for um \n, então será substituido por um \0
	if(r->nome[ult] == '\n')
		r->nome[ult] = '\0';
	r->tamanho = op.tam;
	r->modo = op.modo;
	r->jogadas = nJogadas;

	return r;
}

void imprimeRanking(const Opcoes& op)
{
	int tam;
    //le o ranking salvo do modo de jogo desejado
	ranking* rank = leRanking(op, tam);
    //se há um ranking 
	if(rank != NULL)
    {
        //ordena o ranking de acordo com o numero de jogadas
        ordenaRanking(rank,tam);
    	cout << "\nRanking do modo " << op.tam << "-" << op.modo << endl;
    	for(int i = 0; i < tam; i++)
    		cout << i + 1 << ". " << rank[i].nome << "  Numero de jogadas: "<< rank[i].jogadas << endl;
    	if(tam < 1)
    		cout << "Ainda não existe ranking neste modo." << endl;
    	delete[] rank;
    }
}

void ordenaRanking(ranking* rank, int tam)
{
    //garante que todas as posições foram testadas até que estejam completamente ordenadas 
    for(int i = tam - 1; i > 0; i--)
        //percorre o vetor rank
        for(int j = 0; j < i; j++)
            //se o numero de jogadas for maior que o do que o da proxima posição, então
            //realiza a troca entre essas posições
            if(rank[j].jogadas > rank[j + 1].jogadas)
            {
                ranking qualquer = rank[j];
                rank[j] = rank[j + 1];
                rank[j + 1] = qualquer;
            }
}

bool achouRanking(ranking* vRank, int tam, const ranking& rank)
{
    //percorre todo o vetor
    for(int i = 0; i < tam; i++)
        //se o ranking da posição 'i' for igual ao 'rank' retorna 'true'
        if(!strcmp (vRank[i].nome, rank.nome) && 
            (vRank[i].tamanho == rank.tamanho) && 
            (vRank[i].modo == rank.modo) &&
            (vRank[i].jogadas == rank.jogadas))
            return true;
    //se não achou um ranking igual retorna 'false'
    return false; 
}

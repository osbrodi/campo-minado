#ifndef pontucoes_h
#define pontucoes_h

#include "structs.h"
//le o ranking no arquivo ranking.dat de acordo com o modo de jogo
//o parametro 'tam' recebe o numero de rankings salvos
ranking* leRanking(const Opcoes& op,int &tam);
//função salva um ranking 
void salvaRanking(const ranking& rank, const Opcoes& op);
//cria um novo colocado com nome do jogador, 
//tamanho do campo, modo de jogo e numero de jogadas
ranking* novoRanking(const Opcoes& op, int nJogadas);
//função imprime na tela o ranking de acordo com o modo de jogo
void imprimeRanking(const Opcoes& op);
//função organiza os colocados do rankig em ordem de pontuação
void ordenaRanking(ranking* rank, int tam);
//verifica se já existe um ranking identico ao 'rank'
bool achouRanking(ranking* vRank, int tam, const ranking& rank);
#endif //pontucoes_h

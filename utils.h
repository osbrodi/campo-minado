#ifndef UTILS_H
#define UTILS_H

//Lê um inteiro no intervalo [a,b] com validação de entrada.
int leInt(const char msg[], int a, int b);
//Lê um caracter com validação de entrada
char leChar(const char msg[], const char validos[]);
//obtem o número corresponde a porcentagem
int getNum(int total, int porcento);
//limpa o buffer de entrada (stdin)
void limparBuffer();
//gera um número aleatorio no intervalo [a,b]
int geraAleatorio(int a, int b);
#endif //UTILS_H

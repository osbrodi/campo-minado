/****************************************************************************/
/*                        CAMPO MINADO ORTOGONAL                            */
/*                                                                          */
/*          Autores:                                                        */
/*          Vithor Matteus Amaral De Alcantara  - 17.1.4013                 */
/*          Rodrigo Juliano Da Silva            - 17.1.4050                 */
/*                                                                          */
/*          Professor: Puca Huachi Vaz Penna                                */
/*          Disciplina: BCC201 - Introdução à Computação                    */
/*                                                                          */
/*          Universidade Federal de Ouro Preto - UFOP                       */
/*                                                                          */
/****************************************************************************/

#include <iostream>	//cin, cout
#include <locale>	//setlocale
#include "funcs.h"
#include "utils.h"
#include "pontuacoes.h"

using namespace std;

/******************* Main ***************************/
int main()
{
	setlocale(LC_ALL, "Portuguese");
	bool gameover = false; //true quando o jogador perder
	bool jogar = true;// false quando o jogador não deseja jogar novamente

	while(jogar)
	{
		//imprime o menu e armazena as configurações do jogo
		Opcoes opcoes = menu();
		int tam = getDimensao(opcoes);
		//cria o campo gera as bombas e a contagem de bomabas adjacentes
		Celula **campo = gerarCampo(opcoes);
		//armazena o numero de jogadas
		int njogadas = 0;
		//enquanto o jogador não perder  
		while(!gameover)
		{
			//imprime o campo do jogo
			imprimeCampo(campo, tam, false);
			//le e armazena uma posição
			Pos p = lePosicao(opcoes);
			//enquanto a posição lida já estiver revelada exibe mensagem
			//e le nova posição
			while(campo[p.x][p.y].revelado)
			{
				cout << "A posição já foi revelada!" << endl << endl;
				p = lePosicao(opcoes);
			}
			//abre a posição necessarias e verifica se o jogador perdeu
			gameover = abrirPosicao(campo, tam, p);
			//incrementa o numero de jogadas
			njogadas++;
			//se o jogador ganhar imprime mensagem e finaliza loop
			if(ganhou(campo,opcoes))
			{
				cout <<  "\nPARABÉNS!! VOCÊ GANHOU!!" << endl;
				break;
			}
		}
		//imprime campo com todas posições abertas 
		imprimeCampo(campo, tam, true);
		//se o jogador perder o jogo 
		if(gameover)
		{
			cout << "\nGame Over :(\n";
			//se o modo escolhido não for personalizado,
			if(opcoes.tam != '4' && opcoes.modo != 'P')
				//imprime o ranking do modo jogado
				imprimeRanking(opcoes);
		}
		else//se o jogador ganhou e não for modo personalizado
			if(opcoes.tam != '4' && opcoes.modo != 'P')
			{	
				//salva o ranking do jogador
				ranking* rank = novoRanking(opcoes,njogadas);
				salvaRanking(*rank, opcoes);
				delete[] rank;
				//imprime o ranking do modo jogado
				imprimeRanking(opcoes);
			}

		//verifica se o jogador deseja jogar novamente
		char op = leChar("\n\nDeseja jogar novamente? S para sim ou N para não: ", "SN");
		//se a resposta for negativa, encerra o jogo, caso contario reinicia o jogo
		if(op == 'N')
			jogar = false;
		gameover = false;
		//desaloca a matriz do campo
		deletaCampo(campo, tam+2);
	}
	return 0;
}

#include "utils.h"
#include <iostream>	//cin, cout
#include <stdio.h>	//fflush
#include <stdlib.h>	//rand
#include <ctype.h> //toupper
using namespace std;

#ifdef __linux
	//inclue a biblioteca stdio_ext.h apenas no linux
#include <stdio_ext.h>	//__fpurge
#endif


int leInt(const char msg[], int a, int b)
{
		cout << msg;
		int in;
		//enquanto falhar ou estiver fora do intervalo
		while(!(cin >> in) || (in < a || in > b))
		{
			cin.clear(); //'limpa' os erros do cin
			limparBuffer(); //esvazia o buffer de entrada
			cout << "Valor inválido. " << msg;
		}
		limparBuffer();
		return in;
}

char leChar(const char msg[], const char validos[])
{
	char c;
	cout << msg;
	cin >> c;
	c = toupper(c); //transforma para upper case
	//validação da entrada
	bool valido = false;
	while(!valido)
	{
		int i = 0;
		//verifica se o char digitado é igual a um
		//dos passados no parametro 'validos'
		while(validos[i] != '\0')
		{
			if(validos[i] == c)
				valido = true;
			i++;
		}
		//caso seja invalido, limpa o buffer 
		//retorna uma mensagem de erro e
		//pede para digitar novamente
		if(!valido)
		{
			limparBuffer();
			cout << "Opção inválida. " << msg;
			cin >> c;
			c = toupper(c);
		}
	}
	//se o usuario digitar mais que um char e o primeiro for valido
	//os outros chars ficariam no buffer, podendo ocorrer resultados 
	//indesejados ao longo da execução
	limparBuffer();
	return c;
}

int getNum(int total, int porcento)
{
	return total * (porcento / 100.0);
}

void limparBuffer()
{
	//inclue a função fflush apenas no windows
	//e a função __fpurge apenas no linux
#ifdef _WIN32
    fflush(stdin);
#elif __linux
   __fpurge(stdin);
#endif // _WIN32

}

int geraAleatorio(int a, int b)
{
	//rand() % n, retorna um valor aleatorio entre 0 e n - 1
	//fazendo n = b - a + 1, rand() % n retorna um numero entre
	//0 e b - a, como é necessario um numero entre a e b, soma-se a ao reultado
	return (rand() % (b - a + 1)) + a;
}

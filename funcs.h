#ifndef FUNCS_H
#define FUNCS_H

#include "structs.h"

//imprime o menu e lê as configurações do jogo
Opcoes menu();
//gera as bombas e a contagem de bombas próximas
Celula** gerarCampo(Opcoes& op);
//imprimi a matriz do jogo
void imprimeCampo(Celula **campo,int tam, bool ignoreRevelado);
//retorna a dimensão do campo
int getDimensao(const Opcoes& op);
//le uma posiçao
Pos lePosicao(const Opcoes& op);
//abre uma posicao, retorna true se a posição for uma bomba (gameover)
bool abrirPosicao(Celula **campo,int tam, Pos p);
//aloca uma matriz de Celula
Celula** novoCampo(int tam);
//desaloca uma matriz de Celula
void deletaCampo(Celula** campo, int tam);
//Verifica se ganhou o jogo
bool ganhou(Celula** campo, Opcoes& op);
//permite ao jogador testar o salvamento, carregamento
//e impressão do ranking 
void testarRanking();
#endif //FUNCS_H

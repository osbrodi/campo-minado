#include "funcs.h"
#include "utils.h"
#include "pontuacoes.h"
#include <iostream>	//cin, cout
#include <iomanip>	//setw, setfill
#include <stdlib.h>	//srand
#include <time.h>	//time
#include <ctype.h>	//toupper

using namespace std;

#define Vazio '0'
#define Bomba '*'

Opcoes menu()
{
	Opcoes op;//armazena as configurações do jogo
	cout << "\n\n\n" << setfill('*') << setw(41) << "\n"
		<< "* Bem vindo ao Campo Minado Ortogonal. *\n" 
		<< setw(41) << "*\n"
		<< "\nSelecione o tamanho do tabuleiro:"
		<< "\n[1] 9x9"
		<< "\n[2] 15x15"
		<< "\n[3] 20x20"
		<< "\n[4] Personalizada" 
		<< setfill(' ') << endl;


	op.tam = leChar("Escolha: ", "1234K");

	if(op.tam == '4')//caso o tamanho for personalizado
		//'0x7FFFFFFF' é o maior numero inteiro que um int pode armazenar(famosa gambiarra para adaptar a função)
		op.tam_person = leInt("Digite o tamanho do campo: ",1, 0x7FFFFFFF); 
	else//se tamanho for igual a K
		if(op.tam == 'K')
		{
			//inicia o modo de testes do ranking
			testarRanking();
			exit(1);
		}
	cout << "\nSelecione o modo de jogo."
		<< "\n[F] Modo Fácil (10% de bombas)"
		<< "\n[I] Modo Intermediário (15% de bombas)"
		<< "\n[D] Modo Difícil (20% de bombas)"
		<< "\n[P] Modo Personalizado\n";

	op.modo = leChar("Escolha: ", "IFDP");
	if(op.modo == 'P')//se o modo for personalizado le a porcentagem de bombas
		op.porcent = leInt("Digite a porcentagem de bombas: ", 0,100);

	return op;
}

Celula** gerarCampo(Opcoes& op)
{
	srand(time(NULL)); //coloca a data como seed

	//obtem a dimensão da matriz
	int tam = getDimensao(op);
	Celula** campo = novoCampo(tam+2);

	//inicializa todos os lementos da matriz com vazio e não revelado 
	for(int i = 0; i < tam + 2; i++)
		for(int j = 0; j < tam + 2; j++)
		{
			campo[i][j].conteudo = Vazio;
			campo[i][j].revelado = false;
		}

	//obtem o número de bombas
	switch(op.modo)
	{
		case 'F':
			op.porcent = 10;
			break;
		case 'I':
			op.porcent = 15;
			break;
		case 'D':
			op.porcent = 20;
	}
	int nBombas = getNum(tam * tam,op.porcent);

	//gerar bombas
	for(int i = 0; i < nBombas;)
	{
		//gera as coordenadas onde vai colocar uma bomba
		int x = geraAleatorio(1,tam);
		int y = geraAleatorio(1,tam);
		//se não tem uma bomba na posição gerada
		if(campo[x][y].conteudo != Bomba)
		{
			campo[x][y].conteudo = Bomba;
			i++;

			//se não tem uma bomba nos quadrinhos vizinhos à bomba colocada,
			//e então incrementa essas posições

			//acima
			if(campo[x][y + 1].conteudo != Bomba)
				campo[x][y + 1].conteudo++;
			//abaixo
			if(campo[x][y - 1].conteudo != Bomba)
				campo[x][y - 1].conteudo++;
			//direita
			if(campo[x + 1][y].conteudo != Bomba)
				campo[x + 1][y].conteudo++;
			//esquerda
			if(campo[x - 1][y].conteudo != Bomba)
				campo[x - 1][y].conteudo++;
		}
	}
	return campo;
}

void imprimeCampo(Celula **campo,int tam, bool ignoreRevelado)
{
	//cabeçalho
	cout << setfill(' ') << "\n" << setw(4) << "";
	for(int i = 1; i <= tam; i++)
	{
		cout << setw((tam > 9)?2:1) << i << " ";
	}

	//linha que separa o cabeçalho
	cout << "\n" << setfill('-') << setw((tam > 9)?(tam + 1) * 3 + 1: 4+tam * 2) << "";

	for(int i = 1; i < tam + 1; i++)
	{
		//indices das linhas
		cout << "\n" << setfill(' ') << setw(2) << i <<  " |";
		for(int j = 1; j < tam + 1; j++)
		{
			//conteudo das posições
			cout << setw((tam > 9)?2:1);
			//espaço, quando revelado e Vazio;
			//0, quando não revelado
			//conteúdo, quando revelado e não Vazio
			cout << ((campo[i][j].revelado || ignoreRevelado) ?
				((campo[i][j].conteudo == Vazio) ? ' ' : campo[i][j].conteudo) : '0');

			cout << "|";
		}
	}
	cout << "\n" << endl;
}

int getDimensao(const Opcoes& op)
{
	switch(op.tam)
	{
		case '1':
			return 9;
		case '2':
			return 15;
		case '3':
			return 20;
		case '4':
			return op.tam_person;
		default:
			return 0;
	}
}

Pos lePosicao(const Opcoes& op)
{
   Pos p;
   int tam = getDimensao(op);
   //le a abscissa
   p.x = leInt("Digite o número da linha: ",1, tam);
   //le a ordenada
   p.y = leInt("Digite o número da coluna: ", 1, tam);
   return p;
}

bool abrirPosicao(Celula **campo,int tam, Pos p)
{
	//se não houver uma bomba na posição 
	if(campo[p.x][p.y].conteudo != Bomba)
	{
		//revela a posição
		campo[p.x][p.y].revelado = true;
		//se a posição for vazia
		if(campo[p.x][p.y].conteudo == Vazio)
		{
			int x = p.x, y = p.y; //copia da posição
			//abre as posições a direita até encontrar um numero ou posição já revelada
			while((campo[x][y].conteudo == Vazio) && (x < tam) && !(campo[x + 1][y].revelado))
				campo[++x][y].revelado = true;

			x = p.x;//reseta o valor de x (retorna a posição inicial)
			//abre as posições a esquerda até encontrar um numero ou posição já revelada
			while((campo[x][y].conteudo == Vazio) && (x > 1) && !(campo[x - 1][y].revelado))
				campo[--x][y].revelado = true;

			x = p.x;//reseta o valor de x (retorna a posição inicial)
			//abre as posições abaixo até encontrar um numero ou posição já revelada
			while((campo[x][y].conteudo == Vazio) && (y < tam) && !(campo[x][y + 1].revelado))
				campo[x][++y].revelado = true;

			y = p.y;//reseta o valor de y (retorna a posição inicial)
			//abre as posições acima até encontrar um numero ou posição já revelada
			while((campo[x][y].conteudo == Vazio) && (y > 1) && !(campo[x][y - 1].revelado))
				campo[x][--y].revelado = true;
		}
		//continua jogo
		return false;
	}
	else//perde jogo
		return true;
}

Celula **novoCampo(int tam)
{
	//aloca um vetor de ponteiros
	Celula** mat = new Celula*[tam];
	//percorre as posições do vetor 'mat'
	for(int i = 0; i < tam; i++)
		//aloca um vetor de celula para cada posição do vetor 'mat'
		mat[i] = new Celula[tam];
	return mat;
}

void deletaCampo(Celula** campo, int tam)
{	
	//se campo existir
	if(campo != NULL)
	{
		///percorre as posições do vetor 'campo'
		for(int i = 0; i < tam; i++)
			//se campo na poisção 'i' existir
			if(campo[i] != NULL)
				//desaloca o vetor da posição 'i'
				delete[] campo[i];
		//desaloca o vetor de ponteiros
	 	delete[] campo;
	}
}

bool ganhou(Celula** campo, Opcoes& op)
{
	//obtém a dimensão do campo
	int tam = getDimensao(op);
	//percorre as linhas do campo
	for(int i = 1; i < tam + 1; i++)
		//percorre as colunas do campo
		for(int j = 1; j < tam + 1; j++)
			//se o campo na posição 'i','j' não foi revelado e não houver bombas
			if(!campo[i][j].revelado && campo[i][j].conteudo != Bomba)
				return false;
	return true;
}

void testarRanking()
{
	cout << "\n\n\n" << setfill('*') << setw(24)
		<< "\n" <<"* MODO DEBUG/CHEAT :D *\n" << setw(23) << "*" << setfill(' ') << endl;
	Opcoes op;
	//le as configurações do jogo
	op.tam = leChar("Digite o tamanho: ", "123");
	op.modo = leChar("Digite o modo: ", "IFD");
	int jogadas = leInt("Digite o número de jogadas: ", 0, 0x7FFFFFFF);
	//cria um ranking 
	ranking* rank = novoRanking(op, jogadas);
	//salva o ranking 
	salvaRanking(*rank, op);
	//imprime o ranking do modo de jogo selecionado
	imprimeRanking(op);
}

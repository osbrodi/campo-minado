#ifndef STRUCTS_H
#define STRUCTS_H

struct Opcoes
{
	//armazena o tamanho da matriz 
	//1 para tamanho 9x9 
	//2 para tamanho 15x15
	//3 para tamanho 20x20
	//4 para personalizado
	//OU K PARA TESTE/CHEAT :D
	char tam; 
	//armazena o modo de jogo
	//F para modo facil (10% de bombas)
	//I para modo intermediario ( 15% de bombas)
	//D para o modo dificil (20% de bombas)
	//P para o modo personalizado ('porcent'% de bombas)
	char modo;
	//armazena a porcentagem de bombas 
	//para o modo personalizado
	int porcent;
	//armazena o tamanho da matriz 
	//para o modo perssonalizado
	int tam_person;
};

struct Celula
{
	//armazena a situação da posição
	bool revelado;
	//armazena o conteudo da posição 
	//' ' para posição vazia
	//'*' para posição com bomba
	char conteudo; 
};

struct Pos
{
	int x; //abscissa
	int y; //ordenadas
};

struct ranking
{
	char nome [50]; //Nome do jogador
	char tamanho;   //Tamanho do tabuleiro: 1 (9x9), 2 (15x15) ou 3 (20x20)
	char modo;      //Modo de Jogo: F, I ou D
	int  jogadas;   //Número de jogadas necessárias para ganhar o jogo
};

#endif // STRUCTS_H
